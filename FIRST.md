<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-03-11 11:18:21
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-03-11 11:19:53
 -->

# 章节1 创建虚拟DOM的知识点

**示例**  

```
function hi(){ 
    return (
        <div class="box">
            <h1>hello world</h1>
            <p>welcome to my world, this is jackey chan.</p>
        </div> 
    )
}
```

**说明**  这不是一段浏览器识别的js代码（ 因为直接返回的是个DOM结构，或者说现在还不识别 ）,而是jsx ，React语法糖，这就需要 babel编译，编译过程中需要使用到React有关插件 ' @babel/plugin-transform-react-jsx ' ,以下为编译之后的代码

```
"use strict";

function ok() {
  return React.createElement("div", {
    class: "ok",
    id: "hi"
  }, React.createElement("h1", null, "this is title"));
}
```
**注** babel的配置.babelrc 内容如下 

```
{
  "plugins": [
      ["@babel/plugin-transform-react-jsx", {
          "pragma": "React.createElement"  // pragma 默认为 React.createElement
      }]
  ]
}
```

## React.createElement 官方说明 

创建并返回指定类型的新 React 元素。其中的类型参数既可以是标签名字符串（如 'div' 或 'span'），也可以是 React 组件 类型 （class 组件或函数组件），或是 React fragment 类型。

使用 JSX 编写的代码将会被转换成使用 React.createElement() 的形式。 

**本课程我们将React.createElement替换为自定义的方法'H'**    

#### createElement 源码  ( 这里删除了 if( __DEV__ )的逻辑，无碍阅读 )
```
/**
 * Create and return a new ReactElement of the given type.
 * See https://reactjs.org/docs/react-api.html#createelement
 */
export function createElement(type, config, children) {
  let propName;

  // Reserved names are extracted
  const props = {};

  let key = null;
  let ref = null;
  let self = null;
  let source = null;

  if (config != null) {
    if (hasValidRef(config)) {
      ref = config.ref;
    }
    if (hasValidKey(config)) {
      key = '' + config.key;
    }

    self = config.__self === undefined ? null : config.__self;
    source = config.__source === undefined ? null : config.__source;
    // Remaining properties are added to a new props object
    for (propName in config) {
      if (
        hasOwnProperty.call(config, propName) &&
        !RESERVED_PROPS.hasOwnProperty(propName)
      ) {
        props[propName] = config[propName];
      }
    }
  }

  // Children can be more than one argument, and those are transferred onto
  // the newly allocated props object.
  const childrenLength = arguments.length - 2;
  if (childrenLength === 1) {
    props.children = children;
  } else if (childrenLength > 1) {
    const childArray = Array(childrenLength);
    for (let i = 0; i < childrenLength; i++) {
      childArray[i] = arguments[i + 2];
    }
    props.children = childArray;
  }

  // Resolve default props
  if (type && type.defaultProps) {
    const defaultProps = type.defaultProps;
    for (propName in defaultProps) {
      if (props[propName] === undefined) {
        props[propName] = defaultProps[propName];
      }
    }
  }
  return ReactElement(
    type,
    key,
    ref,
    self,
    source,
    ReactCurrentOwner.current,
    props,
  );
}
```

**源码说明**  
```
 //整体输出 
 ReactElement(
    type,
    key,
    ref,
    self, //__DEV__才会考虑
    source, //__DEV__才会考虑
    ReactCurrentOwner.current,
    props,
  )
```

* 校验 ref、 key、校验属性名（非保留字） ==>> 赋值
* 继续往props上设置属性，将多个children组合成一个，props.children = [多个children]
* 继续往props上添加type的默认属性（ 已添加过的忽略 ）
* 输出ReactElement

#### ReactElement 源码

```
/**
 * 创建一个React 元素，$$typeof 去确定当前是否是React 元素 (虚拟DOM)
 * @param {*} type
 * @param {*} props
 * @param {*} key
 * @param {string|object} ref
 * @param {*} owner
 * @param {*} self __DEV__情况下才会使用，所以这里不翻译了
 * @param {*} source __DEV__情况下才会使用，所以这里不翻译了
 * @internal
 */
const ReactElement = function(type, key, ref, self, source, owner, props) {
  const element = {
    // 识别是否是React元素
    $$typeof: REACT_ELEMENT_TYPE,

    // Built-in properties that belong on the element
    type: type,
    key: key,
    ref: ref,
    props: props,

    // 声明一下该组件属于谁 Record the component responsible for creating this element.
    _owner: owner,
  };
  return element;
};
```