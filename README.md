<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-03-11 10:33:22
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-03-11 20:19:02
 -->
# 前言

> 好记忆不如烂笔头 ————鲁迅爷爷

Coding 的时光也有5、6年了，从最初的html/css/js 到Jquery、AngularJs,再到如今的React，Vue, 又从SSH,到Spring MVC，到至今的SpringBoot/Cloud、Mysql、Docker、Nginx等等...越来越发现脑袋瓜子不够用了，虽然有记笔记，看书划拉的习惯，但是记过的东西总是会忘，重复遇到过的问题也依旧是重复遇到。这是很痛的......索性就写个博客记录一下吧，等哪天忘了起码知道去哪儿找。

书写文档还有个好处是会督促我去深挖原理。

另外后期我会把自己做的电商 前后台代码统统开源，目前还在开发中...

最后，本博客就挂在gitlab上，仓库地址为: https://gitlab.com/kaka-08/ocean 。 还是很希望有同样想法的大哥大姐们一起参与。👏👏👏 

# 参与者 ... 
*  KAKA-08： 一位资深足球迷
*  阿伟： 大四实习生
