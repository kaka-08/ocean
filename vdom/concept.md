<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-03-11 11:17:09
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-03-11 22:03:48
 -->
**了解虚拟DOM**

# 先看看什么是DOM

DOM（Document Object Model——文档对象模型）是用来呈现以及与任意 HTML 或 XML文档交互的API。DOM 是载入到浏览器中的文档模型，，提供了对文档的结构化的表述，并定义了一种方式可以使从程序中对该结构进行访问，从而改变文档的结构，样式和内容。DOM 将文档解析为一个由节点和对象（包含属性和方法的对象）组成的结构集合。简言之，它会将web页面和脚本或程序语言连接起来。本质上是页面的API，允许程序读取和操纵页面的内容，结构和样式，例如我们要获取页面上id为'app'的元素, 则我们可以在js当中这么获取:
```js
    document.getElementById('app')
```
在虚拟DOM 未出现之前，我们一直都是直接操作DOM元素，调用DOM的api去改变DOM结构。

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/DOM-model.svg/1280px-DOM-model.svg.png" width="85%">

上图则是维基百科当中对DOM的描述，DOM

**注** DOM是仅从源HTML文档构建的，不包括应用于元素的样式。所以伪类::before和::after不属于DOM

频繁的去操作DOM是很昂贵的事情，耗CPU, 为什么操作虚拟DOM会昂贵？ 看看这篇文章吧。 https://www.cnblogs.com/padding1015/p/11405788.html

我也整理了一篇浏览器关键路径渲染，理解浏览器在加载一个html文件时候经历了什么  https://www.jianshu.com/p/863fa5eacb17

# 虚拟 DOM

React官方对虚拟DOM的表述如下
>Virtual DOM 是一种编程概念。在这个概念里， UI 以一种理想化的，或者说“虚拟的”表现形式被保存于内存中，并通过如 ReactDOM 等类库使之与“真实的” DOM 同步。这一过程叫做协调。

>这种方式赋予了 React 声明式的 API：您告诉 React 希望让 UI 是什么状态，React 就确保 DOM 匹配该状态。这使您可以从属性操作、事件处理和手动 DOM 更新这些在构建应用程序时必要的操作中解放出来。

>与其将 “Virtual DOM” 视为一种技术，不如说它是一种模式，人们提到它时经常是要表达不同的东西。在 React 的世界里，术语 “Virtual DOM” 通常与 React 元素关联在一起，因为它们都是代表了用户界面的对象。而 React 也使用一个名为 “fibers” 的内部对象来存放组件树的附加信息。上述二者也被认为是 React 中 “Virtual DOM” 实现的一部分。

如果直接看概念可能是一头雾水，从代码的层面上看，虚拟DOM 则是一个简单的JS对象（ plain javaScript Object ），如下

```js
const vdom = {
    tagName: "html",
    children: [
        { tagName: "head" },
        {
            tagName: "body",
            children: [
                {
                    tagName: "ul",
                    attributes: { "class": "list" },
                    children: [
                        {
                            tagName: "li",
                            attributes: { "class": "list__item" },
                            textContent: "List item"
                        }
                    ]
                }
            ]
        } 
    ]
} 
```
可以看出，和DOM类似，虚拟DOM也体现了最终渲染的文档树结构。因为虚拟DOM本身是一个纯粹的对象，而其提供的对比更新机制也是在操作本身，所以在整个操作虚拟DOM的过程当中，我们无需也没有必要考虑到真实的DOM性能，虚拟DOM只有在所有属性对比完成之后才会去更新DOM。




