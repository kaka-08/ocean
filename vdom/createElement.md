<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-03-11 11:17:09
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-03-11 21:57:00
 -->
# 第1节：createElement

这是一个创建虚拟dom的函数。
虚拟dom也就是一个简单的js对象。
其中tagName代表元素的标签，attrs是元素的属性，children是元素的子元素。

```
export default (tagName, { attrs = {},children = [] }) => {
​    return {
​        tagName,
​        attrs,
​        children 
​    }
}
```