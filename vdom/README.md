<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-03-11 14:16:18
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-03-12 10:31:04
 -->

# 代码

**地址** https://gitlab.com/kaka-08/v-dom

**项目有两个分支 , 每个分支我都写了注释**

1. master 分支更容易理解虚拟DOM的diff

2. v1.0.0 可以拓展生命周期钩子 难理解 



本章节当中的diff说明的是master分支的代码，由 阿伟 童鞋整理。

以下是项目效果

<img src='https://feat-assets.oss-cn-beijing.aliyuncs.com/aet/vdom.gif' width=1000>

###### 参考资料

[1][**视频资料**]: https://www.youtube.com/watch?v=85gJMUEcnkc&t=1016s

[2][**代码资料**]: https://github.com/heiskr/prezzy-vdom-example

[3][**更深阅读**]: https://github.com/Matt-Esch/virtual-dom