<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-03-20 15:34:22
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-03-20 15:59:44
 -->

# 类 Class

我们在React/Vue项目当中使用的很多，其实大家对Class已经很熟悉了。先提两个问题，假设我们有一个 class User

**class User {...}, 到底做了什么**

* 创建一个名为 User 的函数，该函数成为类声明的结果。该函数的代码来自于 constructor 方法（如果我们不编写这种方法，那么它就被假定为空）。

* 存储类中的方法，例如 User.prototype 中的 sayHi。

**new User() 的时候 又做了什么**

* 一个新对象被创建

* constructor 使用给定的参数运行，并为其分配 this.name。


**类是不可枚举的，且 类总是使用 use strict**

