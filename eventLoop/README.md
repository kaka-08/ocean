<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-03-20 10:05:52
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-03-20 14:23:40
 -->
# 事件轮训

JavaScript有一个基于事件循环的并发模型，事件循环负责执行代码、收集和处理事件以及执行队列中的子任务。这个模型与其它语言中的模型截然不同，比如 C 和 Java。

<img src="https://mdn.mozillademos.org/files/17124/The_Javascript_Runtime_Environment_Example.svg">

结合该图，聊聊 堆、栈、队列的作用，在理解这些作用的基础上再去分析事件轮训机制

## 堆（Heap）
 
> 对象被分配在堆中，堆是一个用来表示一大块（通常是非结构化的）内存区域的计算机术语。

## 栈 (Stack)

**⭐️⭐️⭐️⭐️⭐️ LIFO( 后进先出 last in first out )**

> 函数的调用形成栈帧，

例如以下代码执行

```js
function foo(b) {
  let a = 10;
  return a + b + 11;
}

function bar(x) {
  let y = 3;
  return foo(x * y);
}

bar(7); // 返回 42

```
大白话： bar() 执行的过程，其实这个过程类似洗碗，总是先洗最上面的一个
* 先将bar丢入栈中
* 准备将bar出栈，发现bar依赖foo，再把foo丢入栈中，foo的未知在bar之上
* 执行foo，出栈（ 因为foo并没有依赖别的函数 ）
* 再执行bar，将bar出栈
* 栈 被清空 


## 队列 （Query）

**⭐️⭐️⭐️⭐️⭐️ FIFO( 先进先出 first in first out )**

一个 JavaScript 运行时包含了一个待处理消息的消息队列。每一个消息都关联着一个用以处理这个消息的回调函数。

在 事件循环 期间的某个时刻，运行时会从**最先进入队列的消息开始处理队列中的消息。被处理的消息会被移出队列**，并作为输入参数来调用与之关联的函数。正如前面所提到的，调用一个函数总是会为其创造一个新的栈帧。

函数的处理会一直进行到执行栈再次为空为止；然后事件循环将会处理队列中的下一个消息（如果还有的话）。

# 事件循环

之所以称之为 事件循环，是因为它经常按照类似如下的方式来被实现：

```js
while (queue.waitForMessage()) {
  queue.processNextMessage();  //会同步地等待消息到达(如果当前没有任何消息等待被处理)。
}
```

## 执行至完成

每一个消息完整地执行后，其它消息才会被执行。这为程序的分析提供了一些优秀的特性，包括：当一个函数执行时，它不会被抢占，只有在它运行完毕之后才会去运行任何其他（哪怕是可以修改此函数中的数据）的代码。这与C语言不同，例如，如果函数在线程中运行，它可能在任何位置被终止，然后在另一个线程中运行其他代码。

这个模型的一个缺点在于当一个消息需要太长时间才能处理完毕时，Web应用程序就无法处理与用户的交互例如点击或滚动。浏览器用“程序需要过长时间运行”的对话框来缓解这个问题。一个很好的做法是缩短消息处理，并在可能的情况下将一个消息裁剪成多个消息。

## 添加消息

在浏览器里，每当一个事件发生并且有一个事件监听器绑定在该事件上时，一个消息就会被添加进消息队列。如果没有事件监听器，这个事件将会丢失。所以当一个带有点击事件处理器的元素被点击时，就会像其他事件一样产生一个类似的消息。

函数 setTimeout 接受两个参数：待加入队列的消息和一个时间值（可选，默认为 0）。这个时间值代表了消息被实际加入到队列的**最小延迟时间**。如果队列中没有其它消息并且栈为空，在这段延迟时间过去之后，消息会被马上处理。但是，如果有其它消息，setTimeout 消息必须等待其它消息处理完。因此第二个参数仅仅表示最少延迟时间，而非确切的等待时间。


下面的例子演示了这个概念(setTimeout 并不会在计时器到期之后直接执行)

```js
const s = new Date().getSeconds();

setTimeout(function() {
  // 输出 "2"，表示回调函数并没有在 500 毫秒之后立即执行
  console.log("Ran after " + (new Date().getSeconds() - s) + " seconds");
}, 500);

while(true) {
  if(new Date().getSeconds() - s >= 2) {
    console.log("Good, looped for 2 seconds");
    break;
  }
}
```

## 零延迟

零延迟并不意味着回调会立即执行。以 0 为第二参数调用 setTimeout 并不表示在 0 毫秒后就立即调用回调函数。

其等待的时间取决于队列里待处理的消息数量。在下面的例子中，"这是一条消息" 将会在回调获得处理之前输出到控制台，这是因为**延迟参数是运行时处理请求所需的最小等待时间**，但并不保证是准确的等待时间。


基本上，setTimeout 需要等待当前队列中所有的消息都处理完毕之后才能执行，即使已经超出了由第二参数所指定的时间。

```js
(function() {

  console.log('这是开始');

  setTimeout(function cb() {
    console.log('这是来自第一个回调的消息');
  });

  console.log('这是一条消息');

  setTimeout(function cb1() {
    console.log('这是来自第二个回调的消息');
  }, 0);

  console.log('这是结束');

})();
```

## 多个运行时互相通信

一个 web worker 或者一个跨域的 iframe 都有自己的栈、堆和消息队列。两个不同的运行时只能通过 postMessage 方法进行通信。如果另一个运行时侦听 message 事件，则此方法会向该运行时添加消息。

# 永不阻塞

JavaScript的事件循环模型与许多其他语言不同的一个非常有趣的特性是，它永不阻塞。 处理 I/O 通常通过事件和回调来执行，所以当一个应用正等待一个 IndexedDB 查询返回或者一个 XHR 请求返回时，它仍然可以处理其它事情，比如用户输入。



# 我们再聊聊 和 栈相关的，也就是 JS的执行上下文

>注意：对于大多数 JavaScript 开发人员来说，这些细节并不重要。这里提供的信息只用于了解为什么微任务非常有用以及它们是如何工作的。如果你并不关心这些内容，你可以跳过这部分或者在你觉得需要的时候再倒回来查看。

当一段 JavaScript 代码在运行的时候，它实际上是运行在执行上下文中。下面3种类型的代码会创建一个新的执行上下文：

* 全局上下文是为运行代码主体而创建的执行上下文，也就是说它是为那些存在于JavaScript 函数之外的任何代码而创建的

* **每个函数会在执行的时候创建自己的执行上下文。这个上下文就是通常说的 “本地上下文”**

* 使用 eval() 函数也会创建一个新的执行上下文

每一个上下文在本质上都是一种作用域层级。每个代码段开始执行的时候都会创建一个新的上下文来运行它，并且在代码退出的时候销毁掉。看看下面这段 JavaScript 程序：

```js
let outputElem = document.getElementById("output");

let userLanguages = {
  "Mike": "en",
  "Teresa": "es"
};

function greetUser(user) {
  function localGreeting(user) {
    let greeting;
    let language = userLanguages[user];
    
    switch(language) {
      case "es":
        greeting = `¡Hola, ${user}!`;
        break;
      case "en":
      default:
        greeting = `Hello, ${user}!`;
        break;
    }
    return greeting;
  }
  outputElem.innerHTML += localGreeting(user) + "<br>\r";
}

greetUser("Mike");
greetUser("Teresa");
greetUser("Veronica");
```
这段程序代码包含了**三个执行上下文**，其中有些会在程序运行的过程中多次创建和销毁。每个上下文创建的时候会被推入**执行上下文栈**。当退出的时候，它会从上下文栈中移除。

* 程序开始运行时，全局上下文就会被创建好

+ + 当执行到 greetUser("Mike") 的时候会为 greetUser() 函数创建一个它的上下文。这个执行上下文会被推入执行上下文栈中。

+ + + 当 greetUser() 调用 localGreeting()的时候会为该方法创建一个新的上下文。并且在 localGreeting() 退出的时候它的上下文也会从执行栈中弹出并销毁。 程序会从栈中获取下一个上下文并恢复执行, 也就是从 greetUser() 剩下的部分开始执行。

+ + + greetUser() 执行完毕并退出，其上下文也从栈中弹出并销毁。

+ + 当 greetUser("Teresa") 开始执行时，程序又会为它创建一个上下文并推入栈顶。

+ + + 当 greetUser() 调用 localGreeting()的时候另一个上下文被创建并用于运行该函数。 当 localGreeting() 退出的时候它的上下文也从栈中弹出并销毁。 greetUser() 得到恢复并继续执行剩下的部分。

+ + + greetUser() 执行完毕并退出，其上下文也从栈中弹出并销毁。

+ + 然后执行到 greetUser("Veronica") 又再为它创建一个上下文并推入栈顶。

+ + + 当 greetUser() 调用 localGreeting()的时候，另一个上下文被创建用于执行该函数。当 localGreeting()执行完毕，它的上下文也从栈中弹出并销毁。

+ + + greetUser() 执行完毕退出，其上下文也从栈中弹出并销毁。

* 主程序退出，全局执行上下文从执行栈中弹出。此时栈中所有的上下文都已经弹出，程序执行完毕。

以这种方式来使用执行上下文，使得每个程序和函数都能够拥有自己的变量和其他对象。每个上下文还能够额外的跟踪程序中下一行需要执行的代码以及一些对上下文非常重要的信息。以这种方式来使用上下文和上下文栈，使得我们可以对程序运行的一些基础部分进行管理，包括局部和全局变量、函数的调用与返回等。

关于递归函数——即多次调用自身的函数，需要特别注意：每次递归调用自身都会创建一个新的上下文。这使得 JavaScript 运行时能够追踪递归的层级以及从递归中得到的返回值，但这也意味着每次递归都会消耗内存来创建新的上下文。

## 任务 vs 微任务

一个任务就是指计划由标准机制来执行的任何 JavaScript，如程序的初始化、事件触发的回调等。 除了使用事件，你还可以使用 setTimeout() 或者 setInterval() 来添加任务。

任务队列和微任务队列的区别很简单，但却很重要：

* 当执行来自任务队列中的任务时，在每一次新的事件循环开始迭代的时候运行时都会执行队列中的每个任务。在每次迭代开始之后加入到队列中的任务需要在下一次迭代开始之后才会被执行.

* 每次当一个任务退出且执行上下文为空的时候，微任务队列中的每一个微任务会依次被执行。不同的是它会等到微任务队列为空才会停止执行——即使中途有微任务加入。换句话说，微任务可以添加新的微任务到队列中，并在下一个任务开始执行之前且当前事件循环结束之前执行完所有的微任务。


接下来写一个demo，考虑一下如何输出

```js

function a(){
  setTimeout(function(){
    console.log('this is a setTimeout')
  },1000);


    Promise.resolve().then(function() {
    console.log('this is a Promise');
  });

  console.log('this is function a')
}

function b(){
  setTimeout(function(){
    console.log('this is b setTimeout')
  });

  Promise.resolve().then(function() {
    console.log('this is b Promise');
  });


  console.log('this is function b')
}

function c(){
  a();
  b();
}

c(); 
```