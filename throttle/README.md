<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-03-11 12:01:18
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-03-18 20:19:08
 -->
# 节流和去抖 实现的核心原理

## 去抖


```js
let timer = null;
/**
 * 去抖, 顾名思义： 去除抖动。。。
 * 使用场景，用户多次点击按钮，只执行最后一次。 可以执行之后置灰操作按钮，提示交互
 * 核心逻辑 setTimeout, 每次点击按钮，清除定时器，重新开始计时  
 * @param { func } callback, the function that need to execute
 * @param { number } wait , default 300ms
 */
 function debounce(callback,wait = 300){
    clearTimeout(timer); //优先清除定时器
    timer = setTimeout(()=>{
        callback();
    }, wait)
}
```

## 节流

```js
/**
 * 节流，节省资源，在某个时间段内允许触发
 * 使用场景 可能是一些动画，比方说检测某些元素的位置，监听位置变化，也比方说射击事件，一直按压键盘发射子弹，要保证
 * 比方说 1秒内 请求均匀请求5次 
 * 核心逻辑 setTimeout ,两次请求的时间间隔 不能小于设置的间隔值
 * @param {function} callback 
 * @param {number} threshhold 
 */
var before = 0; 
function throttle(callback, threshhold = 1000){
    now = new Date().getTime();
    if(now - before < threshhold){ 
        return;
    }
    else{
        before = now;
        setTimeout(function(){
            callback();
        },threshhold)
    }
}
```