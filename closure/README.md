<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-03-18 16:09:32
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-03-18 19:37:24
 -->

* 了解一下闭包，及其实际用处

###### 参考资料

[1][**网站资料**]: https://zh.javascript.info/closure  （ 该文写的很好，希望大家直接看原文，原文下面有很多例子帮助大家理解 JS当中的 {}作用域 ）

## 闭包

我们先看看 [MDN](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Closures) 对闭包的解释

>函数与对其状态即词法环境（lexical environment）的引用共同构成闭包（closure）。也就是说，闭包可以让你从内部函数访问外部函数作用域。在JavaScript，函数在每次创建时生成闭包。

* 是不是看概念会一脸懵。。。,不着急，我们一步步剖析，因为我们知道函数是可以嵌套的，对吧。所以接下来第一步，我们先看看函数嵌套

#### 函数嵌套

```js
function init() {
    var name = "Mozilla"; // name 是一个被 init 创建的局部变量
    function displayName() { // displayName() 是内部函数,一个闭包
        alert(name); // 使用了父函数中声明的变量
    }
    displayName();
}
``` 
执行init函数的效果我们应该都知道，会弹出 'Mozilla',我们平时可能很多时候都在这么coding，比方说我们在函数内部定义了方法来专门转换该函数的参数等等。

* 我们再看一个例子

```js
function makeCounter() {
  let count = 0;

  return function() {
    return count++;
  };
}
```
makeCounter函数返回了一个函数，所以执行makeCounter()得到的结构是一个函数。我们想要知道count的最终值，则需要makeCounter()(); 此时得到结果 0。 我们可以在控制台打印多次试试看
```js
for(let i=0;i<5;i++){ console.log(makeCounter()()) }

```

<img src="https://feat-assets.oss-cn-beijing.aliyuncs.com/aet/%E9%97%AD%E5%8C%851.png" width="750">


我们再执行以下函数,然后看看结果

```js
let getCount = makeCounter();

for(let i=0;i<5;i++){ console.log(getCount()) }
```
<img src="https://feat-assets.oss-cn-beijing.aliyuncs.com/aet/%E9%97%AD%E5%8C%85.png" width="750">

* 是不是很神奇，是不是很有疑惑，带着疑惑继续走。 (ps： 实践加概念会更容易理解，当我们看不懂的时候就继续走例子)

#### 词法作用域（ 也叫词法环境 ）

我们再返回去看 init 函数，解析一下，init 函数在创建时候创建了一个函数作用域，大白话一些，init 函数的大括号包裹的地方就是其作用域。
而在其作用域内部，有变量 "name"，还有函数 "displayName"。而函数引用了其自身作用域外部，同时也是 init 函数作用域内部的变量 "name"。

我们可以把 init 函数的大括号去除掉，此时 name 是全局变量（ window的属性，因为是用var定义的，改成let也可以是全局变量,无妨 ）。此时  displayName 可以引用到 name 变量是没问题的。此时 displayName 的上层作用域是全局。

加上 init函数，displayName 引用 name 也没问题，displayName 的上层作用域 是 init函数。

所以很确定的一点是:
**函数是可以使用其上层作用域的变量的**

调用init函数时候创建的作用域就叫做 **词法作用域（词法环境）**, 以上这个词法作用域的例子描述了引擎是如何解析嵌套函数中的变量的，**词法作用域根据声明变量的位置来确定该变量可被访问的位置**。嵌套函数可**获取**声明于外部作用域的函数

* 注：在 JavaScript 中，每个运行的函数，代码块 {...} 以及整个脚本，都有一个被称为 词法环境（Lexical Environment） 的内部（隐藏）的关联对象。

我们再回到 makeCounter 函数,
第一个例子 makeCounter()() 每次调用都会创建一个词法环境，由于之前的词法环境并没有存储，因此每次都是新增。所以拿到的结果是 0; 

第二个例子 let getCount = makeCounter()； 该语句调用makeCounter函数，并且创建了词法环境，当前词法环境 存储一个嵌套函数 function() {
    return count++;
  }; 和 变量count = 0; 当执行 getCount() 时候，首先会去找寻自身（ 也就是return 的 匿名函数，此时getCount是匿名函数本身 ）的词法环境，发现没有count,则会去上层找 count，此时来到了 makeCounter 创建的词法环境当中，并且给count++; 因此我们在for循环时候能逐个看到count的变化。

* ps: 这其实是函数执行的一种机制

## 再看闭包

>函数与对其状态即词法环境（lexical environment）的引用共同构成闭包（closure）。也就是说，闭包可以让你从内部函数访问外部函数作用域。在JavaScript，函数在每次创建时生成闭包。

是不是有点清晰了，闭包其实是一种现象。函数可以引用外部函数的作用域，且可以总是使用。如上面的例子当中，count 由 0 - 4，就是“总是”使用外部变量的一种现象。

**结合 词法作用域 + 垃圾回收 理解闭包**
函数A 嵌套 函数B, 函数B引用了函数A的变量n，这就形成了一个**闭包** （ **注 函数调用的时候 创建 词法作用域** ）
```js
let a = A(); //保存 词法作用域 A.[Environment] 
```
再执行a，
```js
a() // 创建 词法作用域 "B.[Environment]"
```
B.[Environment] 内部 没有变量n，所以去 A.[Environment] 找 ...

... A Few moment later ...

浏览器将要回收 A.[Environment]，发现 B.[Environment] 有 引用 A.[Environment] 内部的变量n, 所以回收失败

所以我们要在不再使用a()之后，执行以下语句，手动去回收
```js
a = null
```


