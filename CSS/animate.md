<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-03-24 10:39:27
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-03-24 11:08:41
 -->

transition 和 animation

css动画的优点

* 简单的事，简单地做。
* 快速，而且对 CPU 造成的压力很小。

缺点： 

* JavaScript 动画更加灵活。它们可以实现任何动画逻辑，比如某个元素的爆炸效果。
* 不仅仅只是属性的变化。我们还可以在 JavaScript 中生成新元素用于动画。

# transition 过渡，更多关注属性，例如width, left 等等

CSS 过渡的理念非常简单，我们只需要定义某一个属性以及如何动态地表现其变化。当属性变化时，浏览器将会绘制出相应的过渡动画

也就是说：我们只需要改变某个属性，然后所有流畅的动画都由浏览器生成。

CSS transitions 提供了一种在更改CSS属性时控制动画速度的方法。 其可以让属性变化成为一个持续一段时间的过程，而不是立即生效的。比如，将一个元素的颜色从白色改为黑色，通常这个改变是立即生效的，使用 CSS transitions 后该元素的颜色将逐渐从白色变为黑色，按照一定的曲线速率变化。这个过程可以自定义

CSS transitions 可以决定哪些属性发生动画效果 (明确地列出这些属性)，何时开始 (设置 delay），持续多久 (设置 duration) 以及如何动画 (定义timing function，比如匀速地或先快后慢)。

举个例子，以下 CSS 会为 backgroud-color 的变化生成一个 3 秒的过渡动画：

```css
.animated {
  transition-property: background-color;
  transition-duration: 3s;
}
```

* 监听动画是否执行完['transitionend'], WebKit下是 webkitTransitionEnd

```js
el.addEventListener("transitionend", updateTransition, true);
```


# animation 关键帧动画，关注一系列样式配置

CSS animations **使得可以将从一个CSS样式配置转换到另一个CSS样式配置**。动画包括两个部分:描述动画的样式规则和用于指定动画开始、结束以及中间点样式的关键帧。

相较于传统的脚本实现动画技术，使用CSS动画有三个主要优点：

* 能够非常容易地创建简单动画，你甚至不需要了解JavaScript就能创建动画。

* 动画运行效果良好，甚至在低性能的系统上。渲染引擎会使用跳帧或者其他技术以保证动画表现尽可能的流畅。而使用JavaScript实现的动画通常表现不佳（除非经过很好的设计）。

* 让浏览器控制动画序列，允许浏览器优化性能和效果，如降低位于隐藏选项卡中的动画更新频率。

animation的动画设置如下

```css
p {
  animation-duration: 3s;
  animation-name: slidein;
}

@keyframes slidein {
  from {
    margin-left: 100%;
    width: 300%; 
  }

  to {
    margin-left: 0%;
    width: 100%;
  }
}
```

*  可以通过js当中的 event.type 监听动画执行过程['animationstart','animationend','animationiteration']
```js
e.addEventListener("animationstart", listener, false);
```


总结：

transition 关注的是 CSS **property**的变化，property值和时间的关系是一个三次贝塞尔曲线。

animation 作用于元素本身而不是样式属性，可以使用关键帧的概念，应该说可以实现更自由的动画效果。

至于实现动画效果用哪一种，我的感觉是要看应用场景，但很多情况下transition更简单实用些。
