<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-04-29 21:15:14
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-04-29 21:15:25
 -->

 
```js
export default function memo<Props>(
  type: React$ElementType,
  compare?: (oldProps: Props, newProps: Props) => boolean,
) {
  if (__DEV__) {
    if (!isValidElementType(type)) {
      warningWithoutStack(
        false,
        'memo: The first argument must be a component. Instead ' +
          'received: %s',
        type === null ? 'null' : typeof type,
      );
    }
  }
  return {
    $$typeof: REACT_MEMO_TYPE,
    type,
    compare: compare === undefined ? null : compare,
  };
}
```