<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-04-29 17:00:17
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-04-29 17:33:52
 -->
## React.lazy 源码解读

```js
function lazy<T, R>(ctor: () => Thenable<T, R>): LazyComponent<T> {
  let lazyType = {
    $$typeof: REACT_LAZY_TYPE,
    _ctor: ctor,
    // React uses these fields to store the result.
    _status: -1,
    _result: null,
  };

  //这里我把 __DEV__对应的代码删除了，无碍阅读
  
  return lazyType;
}

```