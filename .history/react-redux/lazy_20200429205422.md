<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-04-29 17:00:17
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-04-29 20:54:22
 -->
## React.lazy 源码解读

**前提依赖**

```js
export const Uninitialized = -1;
export const Pending = 0;
export const Resolved = 1;
export const Rejected = 2;
```

**主函数lazy**
```js

function lazy<T, R>(ctor: () => Thenable<T, R>): LazyComponent<T> {
  let lazyType = {
    $$typeof: REACT_LAZY_TYPE,
    _ctor: ctor,
    // React uses these fields to store the result.
    _status: -1,
    _result: null,
  };
  //这里我把 __DEV__对应的代码删除了，无碍阅读
  return lazyType;
}

```
我们接下来把所有相关的源码都读一下,不要太纠结于 flow，感兴趣的话也可以学一下

**Thenable （一个定义了 ` then ` 方法的对象或者函数 )**
> “thenable” is an object or function that defines a then method

```js
export type Thenable<T, R> = {
  then(resolve: (T) => mixed, reject: (mixed) => mixed): R,
};
```

**LazyComponent对象**

```js
export type LazyComponent<T> = {
  $$typeof: Symbol | number,
  _ctor: () => Thenable<{default: T}, mixed>,
  _status: 0 | 1 | 2,
  _result: any,
};
```

**initializeLazyComponentType ( 我把参数校验的代码和__DEV__的代码先删除 )**  ⭐️⭐️⭐️

```js
//源码解读  例如 const OtherComponent = React.lazy(() => import('./OtherComponent')); 这个过程如下: 
export function initializeLazyComponentType(lazyComponent){
    if (lazyComponent._status === Uninitialized) {
        //如果未初始化,则开始初始化，初始化状态为  Pending 
        lazyComponent._status = Pending;
        //lazyComponent 的参数为一个 Thenable，上面说明过： Thenable是一个带then的对象或者方法，可以理解为一个Promise实例
        const ctor = lazyComponent._ctor;  // ()=> { return import('./OtherComponent') }; 
        //执行
        const thenable = ctor();  //结果为 import('./pages/register')
        //修改状态
        lazyComponent._result = thenable;  
        //执行then
        thenable.then(   // import('./pages/register').then( res=>{...} )  此时的 res 为引入的module
            moduleObject => {
                if (lazyComponent._status === Pending) {
                    const defaultExport = moduleObject.default;
                    lazyComponent._status = Resolved;
                    lazyComponent._result = defaultExport;
                }
            },
            error => {
                if (lazyComponent._status === Pending) {
                    lazyComponent._status = Rejected;
                    lazyComponent._result = error;
                }
            },
        );
    }
}
```

**refineResolvedLazyComponent 定义 和 返回 ResolvedLazyComponent** ⭐️⭐️⭐️
```js
export function refineResolvedLazyComponent<T>(
  lazyComponent: LazyComponent<T>,
): ResolvedLazyComponent<T> | null {
  return lazyComponent._status === Resolved ? lazyComponent._result : null;
}
```

**ResolvedLazyComponent 一个对象，包含组件状态**
```js
type ResolvedLazyComponent<T> = {
  $$typeof: Symbol | number,
  _ctor: () => Thenable<{default: T}, mixed>,
  _status: 1,
  _result: any,
};
```


#### 结合官网对 lazy 的功能描述更容易理解源码（ 也可以自己跑测试看看 ）

使用情况
```js
const OtherComponent = React.lazy(() => import('./OtherComponent'));
```
官方说明:
>此代码将会在组件首次渲染时，自动导入包含 OtherComponent 组件的包。

>React.lazy 接受一个函数，这个函数需要动态调用 import()。它必须返回一个 Promise，该 Promise 需要 resolve 一个 defalut export 的 React 组件。


如果仅看这些API，无法将lazy的整个过程串联起来，因为串联的过程还涉及到别的API。这里仅仅阅读的懂源码即可，重点的API都标记了⭐️

先了解基本的API，后面会讲如何串联


**注意： import() 是es6 提供的方法 https://webpack.js.org/api/module-methods/#import-1**