<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-04-29 11:39:24
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-04-29 12:11:57
 -->
## Component 和 PureComponent的源码说明**

依赖
```js
import invariant from 'shared/invariant';
import lowPriorityWarningWithoutStack from 'shared/lowPriorityWarningWithoutStack';

import ReactNoopUpdateQueue from './ReactNoopUpdateQueue';

const emptyObject = {};
```

#### Component 源码

```js

/**
 * 组件更新state过程中的基类
 * 我之前也以为该类实现或者继承了React的生命周期，后来发现并非如此
 * Base class helpers for the updating state of a component.
 */
function Component(props, context, updater) {
  this.props = props;
  this.context = context;
  // If a component has string refs, we will assign a different object later.
  this.refs = emptyObject;
  // We initialize the default updater but the real one gets injected by the
  // renderer.
  this.updater = updater || ReactNoopUpdateQueue;
}

Component.prototype.isReactComponent = {};

```

###### 我们平时使用的setState实则来源于Component
```js 
Component.prototype.setState = function(partialState, callback) {
  invariant(
    typeof partialState === 'object' ||
      typeof partialState === 'function' ||
      partialState == null,
    'setState(...): takes an object of state variables to update or a ' +
      'function which returns an object of state variables.',
  );
  this.updater.enqueueSetState(this, partialState, callback, 'setState');
};
```
setState 注释说明: 
> 我们经常通过setState来改变当前的state，可以把 `this.state`当作是 immutable 的。
没法保证调用setState之后 `this.state`就会立即更新，当我们在调用 setState之后直接去访问 `this.state`，可能拿到的state还是
更新前的值，在这里你可以提供一个回调函数，回调函数会在state被完全更新之后调用（ 未来的某个时间点被调用 ） ，因此回调函数是获取最新state的一个很好的办法。

###### 除了setState之外，源码当中还有一个更新state的方法: forceUpdate 
```js
Component.prototype.forceUpdate = function(callback) {
  this.updater.enqueueForceUpdate(this, callback, 'forceUpdate');
};
```
顾名思义： 强制更新state，但是为了保证理解更准确，还是要再翻译一下注释
>Forces an update. This should only be invoked when it is known with
 * certainty that we are **not** in a DOM transaction.
 *
 * You may want to call this when you know that some deeper aspect of the
 * component's state has changed but `setState` was not called.
 *
 * This will not invoke `shouldComponentUpdate`, but it will invoke
 * `componentWillUpdate` and `componentDidUpdate`.

#### PureComponent 源码
```js

```