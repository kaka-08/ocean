<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-04-29 11:39:24
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-04-29 18:45:28
 -->
## Component 和 PureComponent的源码说明  这里有原型继承的知识点

**依赖**
```js
import invariant from 'shared/invariant';
import lowPriorityWarningWithoutStack from 'shared/lowPriorityWarningWithoutStack';

import ReactNoopUpdateQueue from './ReactNoopUpdateQueue';

const emptyObject = {};
```

**Component 源码**

```js
/**
 * 组件更新state过程中的基类
 * 我之前也以为该类实现或者继承了React的生命周期，后来发现并非如此
 * Base class helpers for the updating state of a component.
 */
function Component(props, context, updater) {
  this.props = props;
  this.context = context;
  // If a component has string refs, we will assign a different object later.
  this.refs = emptyObject;
  // We initialize the default updater but the real one gets injected by the
  // renderer.
  this.updater = updater || ReactNoopUpdateQueue;
}

Component.prototype.isReactComponent = {};
```

**我们平时使用的setState实则来源于Component，你会发现并没有猜测当中的钩子函数**
```js 
Component.prototype.setState = function(partialState, callback) {
  invariant(
    typeof partialState === 'object' ||
      typeof partialState === 'function' ||
      partialState == null,
    'setState(...): takes an object of state variables to update or a ' +
      'function which returns an object of state variables.',
  );
  this.updater.enqueueSetState(this, partialState, callback, 'setState');
};
```
**setState 注释说明**
> 我们经常通过setState来改变当前的state，可以把 `this.state`当作是 immutable 的。
没法保证调用setState之后 `this.state`就会立即更新，当我们在调用 setState之后直接去访问 `this.state`，可能拿到的state还是
更新前的值，在这里你可以提供一个回调函数，回调函数会在state被完全更新之后调用（ 未来的某个时间点被调用 ） ，因此回调函数是获取最新state的一个很好的办法。

**除了setState之外，源码当中还有一个更新state的方法: forceUpdate**
```js
Component.prototype.forceUpdate = function(callback) {
  this.updater.enqueueForceUpdate(this, callback, 'forceUpdate');
};
```
**顾名思义： 强制更新state，但是为了保证理解更准确，还是要再翻译一下注释**
> 强制更新: 调用的前提是 不需要去触发dom事务，也就是说视觉上感知不到state的更新，强制更新会跳过shouldComponentUpdate方法，但是还是会调用componentWillUpdate 和 componentWillUpdate，并且最终执行render()
> 通常你应该避免使用 forceUpdate()，尽量在 render() 中使用 this.props 和 this.state。⭐️⭐️⭐️


**PureComponent 源码**
```js
function PureComponent(props, context, updater) {
  this.props = props;
  this.context = context;
  // If a component has string refs, we will assign a different object later.
  this.refs = emptyObject;
  this.updater = updater || ReactNoopUpdateQueue;
}

const pureComponentPrototype = (PureComponent.prototype = new ComponentDummy());
pureComponentPrototype.constructor = PureComponent;
// Avoid an extra prototype jump for these methods.
Object.assign(pureComponentPrototype, Component.prototype);
pureComponentPrototype.isPureReactComponent = true;

```

###### 这里有一个组件模型函数，用到了原型继承 ComponentDummy

```js
// 原型继承，一定要有一个中间宿主，也可以叫缓冲区
function ComponentDummy() {} 
// 原型继承  ComponentDummy 继承自  Component
ComponentDummy.prototype = Component.prototype;
```

**PureComponent源码解读**
* 函数体的定义和Component一样，都是赋 props、context、refs、updater( 这个后面会说到 )
* PureComponent 的原型 指向  ComponentDummy的实例，最终指向 Component 的原型 （ ComponentDummy很重要，属于原型继承的知识  ）
* 由于原型继承可以改掉 constructor，所以执行 pureComponentPrototype.constructor = PureComponent; 确保PureComponent 的子类constructor指向自己
* Object.assign() 确保 Component.prototype 的所有属性和方法都被 pureComponentPrototype 继承
* 添加 isPureReactComponent 属性  （ 该属性用来标识是否 走  sCU 方法 ）