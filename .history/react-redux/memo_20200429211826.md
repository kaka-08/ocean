<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-04-29 21:15:14
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-04-29 21:18:26
 -->
## memo 

> 如果你的函数组件在给定相同 props 的情况下渲染相同的结果，那么你可以通过将其包装在 React.memo 中调用，以此通过记忆组件渲染结果的方式来提高组件的性能表现。这意味着在这种情况下，React 将跳过渲染组件的操作并直接复用最近一次渲染的结果。

> React.memo 仅检查 props 变更。如果函数组件被 React.memo 包裹，且其实现中拥有 useState 或 useContext 的 Hook，当 context 发生变化时，它仍会重新渲染。

源码如下:
 
```js
export default function memo<Props>(
  type: React$ElementType,
  compare?: (oldProps: Props, newProps: Props) => boolean,
) {
  return {
    $$typeof: REACT_MEMO_TYPE,
    type,
    compare: compare === undefined ? null : compare,
  };
}
```

源码解读: 

其实React的官网已经列举了DEMO来说明memo的使用
```js

```