<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-04-29 17:00:17
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-04-29 18:17:15
 -->
## React.lazy 源码解读

```js

function lazy<T, R>(ctor: () => Thenable<T, R>): LazyComponent<T> {
  let lazyType = {
    $$typeof: REACT_LAZY_TYPE,
    _ctor: ctor,
    // React uses these fields to store the result.
    _status: -1,
    _result: null,
  };
  //这里我把 __DEV__对应的代码删除了，无碍阅读
  return lazyType;
}

```
我们接下来把所有相关的源码都读一下,不要太纠结于 flow，感兴趣的话也可以学一下

#### Thenable “thenable” is an object or function that defines a then method（一个定义了 ` then ` 方法的对象或者函数 ）

```js

export type Thenable<T, R> = {
  then(resolve: (T) => mixed, reject: (mixed) => mixed): R,
};

```

#### LazyComponent对象 。

```js

export type LazyComponent<T> = {
  $$typeof: Symbol | number,
  _ctor: () => Thenable<{default: T}, mixed>,
  _status: 0 | 1 | 2,
  _result: any,
};

```

