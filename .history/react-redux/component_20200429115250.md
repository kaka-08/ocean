<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-04-29 11:39:24
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-04-29 11:52:50
 -->
## Component 和 PureComponent的源码说明**

依赖
```js
import invariant from 'shared/invariant';
import lowPriorityWarningWithoutStack from 'shared/lowPriorityWarningWithoutStack';

import ReactNoopUpdateQueue from './ReactNoopUpdateQueue';

const emptyObject = {};
```

#### Component 源码

```js

/**
 * 组件更新state过程中的基类
 * 我之前也以为该类实现或者继承了React的生命周期，后来发现并非如此
 * Base class helpers for the updating state of a component.
 */
function Component(props, context, updater) {
  this.props = props;
  this.context = context;
  // If a component has string refs, we will assign a different object later.
  this.refs = emptyObject;
  // We initialize the default updater but the real one gets injected by the
  // renderer.
  this.updater = updater || ReactNoopUpdateQueue;
}

Component.prototype.isReactComponent = {};


```