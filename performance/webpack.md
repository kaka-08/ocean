<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-03-22 09:18:52
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-03-22 22:40:35
 -->
# webpack 可以做的性能优化

* 本文说明的都是webpack4x的配置，webpack4以前的配置差异很大，但是核心的优化还是不变哈。

一年前大致写过webpack4x的一些配置，链接在此，https://github.com/kaka-08/webpack4x-v1;

**在平时工作当中其实我们使用的更多还是一些开源脚手架 比如[create-react-app](https://github.com/facebook/create-react-app),[react-boilerplate](https://github.com/react-boilerplate/react-boilerplate) 等等。但脚手架不是万能的，更多的拓展工作还需要我们了解一下，比方说gzip,比方说Code Splitting,脚手架也提供了拓展配置的插件，比方说create-react-app的[customize-cra](https://github.com/arackaf/customize-cra),我们可以在脚手架的基础上做一些性能方面的优化**

文章不会分析脚手架帮我们做了什么，而我们需要基于某个脚手架再去做什么拓展，因为脚手架的使用和配置互相都会有差异，所以这里只将webpack4可以做什么优化。

webpack大致做的优化有三点： 

* 压缩代码（ 减少包体积 ）

* 缓存

* 监控+分析（ 为了更多的压缩和缓存 ）

## 压缩代码，或者叫 减小包体积

#### 使用 mode, 例如 mode: development 或者 mode: production。 生产模式一定要 mode: production

我们看看 development 和 production 的mode下 webpack4分别默认帮我们做了什么

**mode: development**

* 浏览器调试工具

* 快速增量编译以加快开发周期

* 运行时有用的错误消息

**mode: production**

* 压缩体积 ⭐

* 运行时代码更快

* 去除仅在开发模式下才使用的代码

* 不展示源码和文件路径

* 静态资源更方便使用

更多可以读这里  https://medium.com/webpack/webpack-4-mode-and-optimization-5423a6bc597a

#### 去除空格，缩短代码名称

```js
// 源码
function map(array, iteratee) {
  let index = -1;
  const length = array == null ? 0 : array.length;
  const result = new Array(length);

  while (++index < length) {
    result[index] = iteratee(array[index], index, array);
  }
  return result;
}
```
经过缩小之后
```js
function map(n,r){let t=-1;for(const a=null==n?0:n.length,l=Array(a);++t<a;)l[t]=r(n[t],t,n);return l}
```

###### 包级别的压缩 关键词 **UglifyJs**

配置如下：

```js
const webpack = require('webpack');

module.exports = {
  plugins: [
    new webpack.optimize.UglifyJsPlugin(),
  ],
};
```

压缩前： 
```js
import './comments.css';
export function render(data, target) {
  console.log('Rendered!');
}
```
编译之后（未压缩）
```js
"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["render"] = render;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__comments_css__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__comments_css_js___default =
__webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__comments_css__);

function render(data, target) {
  console.log('Rendered!');
}
```
压缩后
```js
"use strict";function t(e,n){console.log("Rendered!")}
Object.defineProperty(n,"__esModule",{value:!0}),n.render=t;var o=r(1);r.n(o)
```

###### loader配置 
我们知道，css-loader是可以配置压缩css代码的哈
例如：
```js
{
    test: /\.css$/,
    use: [
        'style-loader',
        { loader: 'css-loader', options: { minimize: true } }, //css-loader 如今已经不支持 minimize了。
    ],
},
```

###### 指定 NODE_ENV=production

作用是 将所有的 process.env.NODE_ENV 都赋值为 'production'
```js
module.exports = {
  optimization: {
    nodeEnv: 'production',
    minimize: true,
  },
};
```

#### 使用 es Module

使用 es Module的好处是  可以利用webpack 的 tree-shaking（ 会识别和删除未引用代码 ）

** In webpack, tree-shaking doesn’t work without a minifier. Webpack just removes export statements for exports that aren’t used; it’s the minifier that removes unused code. Therefore, if you compile the bundle without the minifier, it won’t get smaller.

tree-shaking 的更多介绍还是看webpack的官网好一些 https://webpack.docschina.org/guides/tree-shaking/#src/components/Sidebar/Sidebar.jsx


#### 优化图片

```js
    rules: [
      {
        test: /\.(jpe?g|png|gif)$/,
        loader: 'url-loader',
        options: {
          // Inline files smaller than 10 kB (10240 bytes)
          limit: 10 * 1024,
        },
      },
    ],
```
如果图片小于10KB,则 base64,如果大于，则丢到static文件里面

#### 优化第三方库

https://github.com/GoogleChromeLabs/webpack-libs-optimizations  参考这里做优化

#### 使用 ES modules

使用 ES modules 有个好处， concat enate Modules,( 将相关的模块合并成一个 )，例如，目标modules:

```js
// index.js
import {render} from './comments.js';
render();

// comments.js
export function render(data, target) {
  console.log('Rendered!');
}
```

webpack将其打包成CommonJS格式,如下
```js
(function(module, __webpack_exports__, __webpack_require__) {

  "use strict";
  Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
  var __WEBPACK_IMPORTED_MODULE_0__comments_js__ = __webpack_require__(1);
  Object(__WEBPACK_IMPORTED_MODULE_0__comments_js__["a" /* render */])();

}),
/* 1 */
(function(module, __webpack_exports__, __webpack_require__) {

  "use strict";
  __webpack_exports__["a"] = render;
  function render(data, target) {
    console.log('Rendered!');
  }

})
```

打包成es Modules
```js
(function(module, __webpack_exports__, __webpack_require__) {

  "use strict";
  Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

  // CONCATENATED MODULE: ./comments.js
  function render(data, target) {
    console.log('Rendered!');
  }

  // CONCATENATED MODULE: ./index.js
  render();

})
```

对应的webpack配置如下
```js
module.exports = {
  optimization: {
    concatenateModules: true,
  },
};
```

#### externals 

将不希望打包的文件或者库直接采用 CDN或者 文件引入的格式，比如jq，比如React,配置如下

* 引入jquery
```js
<script
  src="https://code.jquery.com/jquery-3.1.0.js"
  integrity="sha256-slogkvB1K3VOkzAI8QITxV3VzpOnkeNVsKvtkYLMjfk="
  crossorigin="anonymous">
</script>
```
*  配置webpack

```js
externals: {
    jquery: 'jQuery'
  }
```
如何配置可以看这里  https://webpack.docschina.org/guides/author-libraries

## 缓存

* 浏览器在请求服务器资源时候，例如请求index.js，如果我们的文件名未发生改变，浏览器会从缓存当中读取文件；如果文件名发生改变，浏览器会重新去下载资源（ 缓存时期到期之后也会去请求）

不经过webpack打包的文件，如果我们想要更新缓存，可以设置版本号。例如index.v1.js 改为 index.v2.js。其实在webpack当中不会使用版本号来更新缓存，而是**chunkhash**

#### 使用 cache headers 和 chunkhash
```js
module.exports = {
  entry: './index.js',
  output: {
    filename: 'bundle.[chunkhash].js',
        // → bundle.8e0d62a03.js
  },
};
```

#### 切割文件 ( 每次编译，修改过的文件的chunkhash会改变，不改的文件，浏览器会取缓存 )

```js
module.exports = {
  optimization: {
    splitChunks: {
      chunks: 'all',
    }
  },
};
```

#### Webpack runtime code

仅仅做代码拆分还不够，如果修改某些代码，你会发现每次vendor.[chunkhash].js 的chunkhash也会改变，哪怕你没有修改vender关联的内容，这是因为vendor.js当中包含runtime代码，runtime.js当中保存着每次打包的chunkhash，所以vendor也会改变。所以我们要做的是把runtime代码抽离出来。
```js
module.exports = {
  optimization: {
    runtimeChunk: true,
  },
};
```

#### 懒加载 + Split the code into routes and pages

关键词： import()

## 监控和分析

####  webpack-dashboard

这个插件 在 create-react-app当中，经尝试，无果，不过效果很赞

<img src="https://developers.google.com/web/fundamentals/performance/webpack/webpack-dashboard.png?hl=zh-cn">

#### webpack-bundle-analyzer

集成很简单

```js
// webpack.config.js
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
  plugins: [
    new BundleAnalyzerPlugin(),
  ],
};
```

<img src="https://feat-assets.oss-cn-beijing.aliyuncs.com/aet/fenxi.png">