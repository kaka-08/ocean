<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-04-29 21:41:04
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-05-09 21:42:54
 -->
## ReactDOM.render是我们常见的API,我们来读一下其源码，为了方便大家的习惯，代码当中我尽量删除了flow的语法，加入了大家熟悉的注释

```js
//需要知道的一些常量
export const LegacyRoot = 0;
export const BatchedRoot = 1;
export const ConcurrentRoot = 2;

export type RootTag = 0 | 1 | 2;  // RootTag的值 可能是 0 或者 1 或者 2 


const ReactDOM = {
    // ... 其他函数
    render(
        element: React$Element<any>,  // React$Element 元素
        container: DOMContainer,  // DOM元素，下面会针对是否为DOM元素去做校验
        callback: ?Function,  //回调函数，传递给 legacyRenderSubtreeIntoContainer
    ) {
        invariant(
            isValidContainer(container),
                'Target container is not a DOM element.',
            );
        return legacyRenderSubtreeIntoContainer(
            null,
            element,
            container,
            false,
            callback,
        );
    },
}

```

## legacyRenderSubtreeIntoContainer 源码

```js
/**
* @param { React$Component }  parentComponent 纯React组件
* @param { ReactNodeList } children  React node 列表，例如: jsx语法
* @param { DOMContainer } container  目标DOM，document.getElementById('root'); 等
* @param { boolean } boolean 是否聚合 
* @param { Function } callback 回调函数
* @return { Function } getPublicRootInstance 
*/
function legacyRenderSubtreeIntoContainer(
    parentComponent,
    children,
    container,
    forceHydrate,
    callback,
    ) {
        //如果 container 包含_reactRootContainer 属性, TODO 该属性哪儿来
        let root = container._reactRootContainer;
        let fiberRoot;
        if (!root) {
            // SPA项目当中 ReactDOM.reander(<App />, document.getELementById('root')) 会走此逻辑
            root = container._reactRootContainer = legacyCreateRootFromDOMContainer(
                container,
                forceHydrate,
            );
            // 创建root成功之后 定义 fiberRoot 
            fiberRoot = root._internalRoot;
            // 上面的流程同样没有 callback, TODO 
            if (typeof callback === 'function') {
                const originalCallback = callback;
                callback = function() {
                    const instance = getPublicRootInstance(fiberRoot);
                    originalCallback.call(instance);
                };
            }
            // 第一次挂载，不应该批量处理，应该一次性处理完成 TODO
            unbatchedUpdates(() => {
                updateContainer(children, fiberRoot, parentComponent, callback);
            });
        } else {
            fiberRoot = root._internalRoot;
            if (typeof callback === 'function') {
                const originalCallback = callback;
                callback = function() {
                    const instance = getPublicRootInstance(fiberRoot);
                    originalCallback.call(instance);
                };
            }
            // 更新 container
            updateContainer(children, fiberRoot, parentComponent, callback);
        }
    return getPublicRootInstance(fiberRoot);
}
```


## 创建 legacyCreateRootFromDOMContainer API  这么命名，可能将来版本有改动？

**方法名字拆分 根据 DOM 创建 ROOT 的遗留 的方法**
```js
/**
* @param { DOMContainer } container : 真实的DOM容器
* @param { forceHydrate } forceHydrate : 是否聚合
* @return { _ReactRoot } 返回react root: 创建根元素
*/
function legacyCreateRootFromDOMContainer(
  container,
  forceHydrate,
){
  const shouldHydrate =
     forceHydrate || shouldHydrateDueToLegacyHeuristic(container);
    if (!shouldHydrate) {
        let warned = false;

        let rootSibling;
        // 因为此时是创建Root，因此要把container当中的所有元素都清除掉
        while (rootSibling = container.lastChild) {
            // ⭐️⭐️⭐️  这里给大家写一个demo，从来没这么玩过
            container.removeChild(rootSibling);
        }
    }

    // Legacy roots are not batched.
    // 看得出来将来还会有改动
    return new ReactBlockingRoot(
        container,
        LegacyRoot,
        shouldHydrate
        ? 
        {
            hydrate: true,
        }
        : undefined,
    );
}

```

## ReactBlockingRoot 

**给当前的调用对象设置 _internalRoot**

```js
/**
* @param { DOMContainer } container
* @param { RootTag } tag
* @param { void || RootOptions } options
*/
function ReactBlockingRoot(
  container,
  tag,
  options,
) {
  this._internalRoot = createRootImpl(container, tag, options);
}
```

## 创建Root
```js
/**
*
* @param {  DOMContainer } container: DOM容器
* @param {  RootTag } tag: RootTag 0|1|2
* @param {  RootOptions } options: 创建Root的参数
*/
function createRootImpl(
  container,
  tag,
  options,
) {
    // Tag 或者是 LegacyRoot 或者是  ConcurrentRoot 也就是说 0 或者 2
    const hydrate = options != null && options.hydrate === true;
    const hydrationCallbacks = (options != null && options.hydrationOptions) || null;
    // 创建Container
    const root = createContainer(container, tag, hydrate, hydrationCallbacks);

    // 创建完root之后，给root打标记 
    markContainerAsRoot(root.current, container);
    //
    if (hydrate && tag !== LegacyRoot) {
        const doc =
        container.nodeType === DOCUMENT_NODE
            ? container
            : container.ownerDocument;
        eagerlyTrapReplayableEvents(doc);
    }
    return root;
}
```

## 创建Container的源码  实则创建 FiberRoot

```js

export function createContainer(
  containerInfo: Container,
  tag: RootTag,
  hydrate: boolean,
  hydrationCallbacks: null | SuspenseHydrationCallbacks,
): OpaqueRoot {
  return createFiberRoot(containerInfo, tag, hydrate, hydrationCallbacks);
}

/**
* 创建 根Fiber
* @param { Container } containerInfo
* @param { number } tag  0,1,2
* @param { boolean } hydrate 
* @param { function } hydrationCallbacks  SuspenseHydrationCallbacks 或者 null
* @return { FiberRoot } 
*/
export function createFiberRoot(
        containerInfo,
        tag,
        hydrate,
        hydrationCallback,
    ) {
        //创建root
        const root = new FiberRootNode(containerInfo, tag, hydrate);
        if (enableSuspenseCallback) {
            // 如果支持 SuspenseCallback  TODO Suspense 是下一个阶段要读的东西，这里先做阅读
            root.hydrationCallbacks = hydrationCallbacks;
        }

        // Cyclic construction. This cheats the type system right now because stateNode is any.
        
        const uninitializedFiber = createHostRootFiber(tag);

        root.current = uninitializedFiber;

        uninitializedFiber.stateNode = root;

        return root;
    }


// Cyclic construction. This cheats the type system right now because stateNode is any.
/**
* @param { RootTag } tag 实际上是number 0,1,2 
* @return { Fiber }
*/
export function createHostRootFiber(tag) {
    let mode;
    if (tag === ConcurrentRoot) {
        mode = ConcurrentMode | BlockingMode | StrictMode;
    } else if (tag === BatchedRoot) {
        mode = BlockingMode | StrictMode;
    } else {
        mode = NoMode;
    }

    // 和性能分析有关，不影响阅读源码
    if (enableProfilerTimer && isDevToolsPresent) {
        // Always collect profile timings when DevTools are present.
        // This enables DevTools to start capturing timing at any point–
        // Without some nodes in the tree having empty base times.
        mode |= ProfileMode;
    }

  return createFiber(HostRoot, null, null, mode);
}


export type TypeOfMode = number;

export const NoMode = 0b0000;
export const StrictMode = 0b0001;
export const BlockingMode = 0b0010;
export const ConcurrentMode = 0b0100;
export const ProfileMode = 0b1000;


/**
* 创建Fiber
* @param { WorkTag } tag 实则也是 number     export type WorkTag = number;
* @param { mixed } pendingProps  //混合，且必须有校验属性
* @param { null || string } key
* @param { TypeOfMode } mode  实则为数字 0 1 2 4 8 
* @return { Fiber } 
*/ 
const createFiber = function(
  tag,
  pendingProps,
  key,
  mode,
) {
  // $FlowFixMe: the shapes are exact here but Flow doesn't like constructors
  return new FiberNode(tag, pendingProps, key, mode);
};

// 普通的Fiber 节点
function FiberNode(
  tag,
  pendingProps,
  key,
  mode,
) {
  // Instance
  this.tag = tag;
  this.key = key;
  this.elementType = null;
  this.type = null;
  this.stateNode = null;

  // Fiber
  this.return = null;
  this.child = null;
  this.sibling = null;
  this.index = 0;

  this.ref = null;

  this.pendingProps = pendingProps;
  this.memoizedProps = null;
  this.updateQueue = null;
  this.memoizedState = null;
  this.dependencies = null;

  this.mode = mode;

  // Effects
  this.effectTag = NoEffect;
  this.nextEffect = null;

  this.firstEffect = null;
  this.lastEffect = null;

  this.expirationTime = NoWork;
  this.childExpirationTime = NoWork;

  this.alternate = null;

  //可以忽略，检测性能
  if (enableProfilerTimer) {
    // Note: The following is done to avoid a v8 performance cliff.
    //
    // Initializing the fields below to smis and later updating them with
    // double values will cause Fibers to end up having separate shapes.
    // This behavior/bug has something to do with Object.preventExtension().
    // Fortunately this only impacts DEV builds.
    // Unfortunately it makes React unusably slow for some applications.
    // To work around this, initialize the fields below with doubles.
    //
    // Learn more about this here:
    // https://github.com/facebook/react/issues/14365
    // https://bugs.chromium.org/p/v8/issues/detail?id=8538
    this.actualDuration = Number.NaN;
    this.actualStartTime = Number.NaN;
    this.selfBaseDuration = Number.NaN;
    this.treeBaseDuration = Number.NaN;

    // It's okay to replace the initial doubles with smis after initialization.
    // This won't trigger the performance cliff mentioned above,
    // and it simplifies other profiler code (including DevTools).
    this.actualDuration = 0;
    this.actualStartTime = -1;
    this.selfBaseDuration = 0;
    this.treeBaseDuration = 0;
  }

  // This is normally DEV-only except www when it adds listeners.
  // TODO: remove the User Timing integration in favor of Root Events.
  //可以忽略，检测性能
  if (enableUserTimingAPI) {
    this._debugID = debugCounter++;
    this._debugIsCurrentlyTiming = false;
  }
}


// 根Fiber  以下常量希望不影响阅读
// 前提须知:  const NoWork = 0;
// 前提须知:  const noTimeout = -1;
// 前提须知:  const NoPriority = 90;
// 前提须知:  const NoTimeout = -1;
function FiberRootNode(containerInfo, tag, hydrate) {
  this.tag = tag;
  this.current = null;
  this.containerInfo = containerInfo;
  this.pendingChildren = null;
  this.pingCache = null;
  this.finishedExpirationTime = NoWork;
  this.finishedWork = null;
  this.timeoutHandle = noTimeout;
  this.context = null;
  this.pendingContext = null;
  this.hydrate = hydrate;
  this.callbackNode = null;
  this.callbackPriority = NoPriority;
  this.firstPendingTime = NoWork;
  this.firstSuspendedTime = NoWork;
  this.lastSuspendedTime = NoWork;
  this.nextKnownPendingLevel = NoWork;
  this.lastPingedTime = NoWork;
  this.lastExpiredTime = NoWork;

  if (enableSchedulerTracing) {
    this.interactionThreadID = unstable_getThreadID();
    this.memoizedInteractions = new Set(); //TODO 为了缓存
    this.pendingInteractionMap = new Map(); //TODO 为了缓存
  }
  if (enableSuspenseCallback) {
    this.hydrationCallbacks = null;
  }
}

```

## 批量删除某元素的方法，从来没这么玩过

```js
function batchDel(arrs){ 
    let temp;  
    let len = arrs.length;
    while( temp = arrs[len-1] ){
        arrs.pop();
        len = arrs.length;
    }
    return arrs;
}
```