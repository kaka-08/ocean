<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-03-20 16:00:32
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-03-20 16:30:08
 -->

# F.prototype

如果 F.prototype 是一个对象，那么 new 操作符会使用它为新对象设置 **[[Prototype]]**。

>JavaScript 从一开始就有了原型继承。这是 JavaScript 编程语言的核心特性之一。

>但是在过去，没有直接对其进行访问的方式。唯一可靠的方法是本章中会介绍的构造函数的 "prototype" 属性。目前仍有许多脚本仍在使用它。


## 默认的 F.prototype，构造函数属性

每个函数都有 "prototype" 属性，即使我们没有提供它。

默认的 "prototype" 是一个只有属性 constructor 的对象，属性 constructor 指向函数自身。

像这样：

```js
function Rabbit() {}

/* default prototype
Rabbit.prototype = { constructor: Rabbit };
*/
```

**prototype 和 \_\_proto\_\_不同 ,后者是继承，指向自己的父级**


**JavaScript 自身并不能确保正确的 "constructor" 函数值**,我们可以修改constructor,例如

```js
function Rabbit() {}
Rabbit.prototype = {
  jumps: true
};

let rabbit = new Rabbit();
alert(rabbit.constructor === Rabbit); // false
```

因此，为了确保正确的 "constructor"，我们可以选择添加/删除属性到默认 "prototype"，而不是将其整个覆盖：

```js
function Rabbit() {}

// 不要将 Rabbit.prototype 整个覆盖
// 可以向其中添加内容
Rabbit.prototype.jumps = true
// 默认的 Rabbit.prototype.constructor 被保留了下来
```

或者，也可以手动重新创建 constructor 属性：

```js
Rabbit.prototype = {
  jumps: true,
  constructor: Rabbit
};

// 这样的 constructor 也是正确的，因为我们手动添加了它
```

总结

* F.prototype 属性（不要把它与 **[[Prototype]]** 弄混了）在 new F 被调用时为新对象的 **[[Prototype]]** 赋值。

* F.prototype 的值要么是一个对象，要么就是 null：其他值都不起作用。

* "prototype" 属性仅在设置了一个构造函数（constructor function），并通过 new 调用时，才具有这种特殊的影响。


>F.prototype 属性仅在 new F 被调用时使用，它为新对象的 **[[Prototype]]** 赋值。之后，F.prototype 和新对象之间就没有任何联系了。可以把它看成“一次性的礼物”。

>如果在创建之后，F.prototype 属性有了变化（F.prototype = <another object>），那么通过 new F 创建的新对象也将随之拥有新的对象作为 **[[Prototype]]**，但**已经存在的对象将保持旧有的值**。

我们可以看两个例子，加深理解和记忆

```js
function Rabbit() {}
Rabbit.prototype = {
  eats: true
};

let rabbit = new Rabbit();

Rabbit.prototype = {};
alert( rabbit.eats ); // 这里会弹出什么 ，记住上面的话： 如果在创建之后，F.prototype 属性有了变化（F.prototype = <another object>），那么通过 new F 创建的新对象也将随之拥有新的对象作为 [[Prototype]]，但已经存在的对象将保持旧有的值。
```

再来一个

```js
function Rabbit() {}
Rabbit.prototype = {
  eats: true
};

let rabbit = new Rabbit();

Rabbit.prototype.eats = false;
alert( rabbit.eats ); //这里会弹出什么呢？

```