<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-03-20 14:25:29
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-03-20 19:52:46
 -->

# 原型继承 

[1][**文章来源**] (https://zh.javascript.info/prototype-inheritance)

* 注: 这篇文章翻译的很细致，不久前还有很多漏洞，如今看来已经很完美，就照搬啦

#### [[Prototype]]

在 JavaScript 中，对象有一个特殊的隐藏属性 **[[Prototype]]**（如规范中所命名的），它要么为 null，要么就是对另一个对象的引用。该对象被称为“原型”：

原型有点“神奇”。当我们想要从 object 中读取一个缺失的属性时，JavaScript 会自动从原型中获取该属性。在编程中，这种行为被称为“原型继承”。许多炫酷的语言特性和编程技巧都基于此。

属性 **[[Prototype]]** 是内部的而且是隐藏的，但是这儿有很多设置它的方式。

其中之一就是使用特殊的名字, \_\_proto\_\_

下面是一段原型继承的代码
```js
let animal = {
  eats: true
};
let rabbit = {
  jumps: true,
  __proto__: animal
};

```
此时，rabbit 就有了 animal 的 eats 属性

>\_\_proto\_\_ 是 [[Prototype]] 的因历史原因而留下来的 getter/setter
请注意，\_\_proto\_\_ 与 [[Prototype]] 不一样。\_\_proto\_\_ 是 [[Prototype]] 的 getter/setter。

>\_\_proto\_\_ 的存在是历史的原因。在现代编程语言中，将其替换为函数 Object.getPrototypeOf/Object.setPrototypeOf 也能 get/set 原型。我们稍后将学习造成这种情况的原因以及这些函数。

>根据规范，\_\_proto\_\_ 必须仅在浏览器环境下才能得到支持，但实际上，包括服务端在内的所有环境都支持它。目前，由于 \_\_proto\_\_ 标记在观感上更加明显，所以我们在后面的示例中将使用它。


在这儿我们可以说 "animal 是 rabbit 的原型"，或者说 "rabbit 的原型是从 animal 继承而来的"。

因此，如果 animal 有许多有用的属性和方法，那么它们将自动地变为在 rabbit 中可用。这种属性被称为“继承”。

如果我们在 animal 中有一个方法，它可以在 rabbit 中被调用：

```js
let animal = {
  eats: true,
  walk() {
    alert("Animal walk");
  }
};

let rabbit = {
  jumps: true,
  __proto__: animal
};

// walk 方法是从原型中获得的
rabbit.walk(); // Animal walk
```

再来看一个例子，关于this的问题

```js
let user = {
  name: "John",
  surname: "Smith",

  set fullName(value) {
    [this.name, this.surname] = value.split(" ");
  },

  get fullName() {
    return `${this.name} ${this.surname}`;
  }
};

let admin = {
  __proto__: user,
  isAdmin: true
};
```

在控制台执行
```js
console.log(admin.fullName);

admin.fullName = "John Kaka";

console.log(admin.fullName);

```

在上面的例子中可能会出现一个有趣的问题：在 **set fullName(value)** 中 this 的值是什么？属性 this.name 和 this.surname 被写在哪里：在 user 还是 admin？

答案是： this 根本不受原型的影响。

记住一句话： **无论在哪里找到方法：在一个对象还是在原型中。在一个方法调用中，this 始终是点符号 . 前面的对象。**


再来一个例子，查看属性遍历

```js
let animal = {
  eats: true
};

let rabbit = {
  jumps: true,
  __proto__: animal
};
```


两个注意点：

* Object.keys(rabbit) 为["jump"]： **Object.keys 只返回自己的 key**
* for(let k in rabbit){ console.log(k) } 会打印"jump","eats"： **for..in 会遍历自己以及继承的键**

如果我们只想使用对象自己的属性，排除继承的属性，除了Object.keys()之外，也可以使用 Object.hasOwnProperty.call(target); 例如： rabbit.hasOwnProperty("jump") 


总结
 
* 在 JavaScript 中，所有的对象都有一个隐藏的 [[Prototype]] 属性，它要么是另一个对象，要么就是 null。

* 我们可以使用 obj.\_\_proto\_\_ 访问它（历史遗留下来的 getter/setter，这儿还有其他方法，很快我们就会讲到）。

* 通过 [[Prototype]] 引用的对象被称为“原型”。

* 如果我们想要读取 obj 的一个属性或者调用一个方法，并且它不存在，那么 JavaScript 就会尝试在原型中查找它。

* 写/删除操作直接在对象上进行，它们不使用原型（假设它是数据属性，不是 setter）。

* 如果我们调用 obj.method()，而且 method 是从原型中获取的，this 仍然会引用 obj。因此，方法始终与当前对象一起使用，即使方法是继承的。

* for..in 循环在其自身和继承的属性上进行迭代。所有其他的键/值获取方法仅对对象本身起作用。


结尾处放个例子，加深理解, 为什么两只仓鼠都饱了？

```js
let hamster = {
  stomach: [],

  eat(food) {
    this.stomach.push(food);
  }
};

let speedy = {
  __proto__: hamster
};

let lazy = {
  __proto__: hamster
};

// 这只仓鼠找到了食物
speedy.eat("apple");
alert( speedy.stomach ); // apple

// 这只仓鼠也找到了食物，为什么？请修复它。
alert( lazy.stomach ); // apple

```

