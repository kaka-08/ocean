<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-03-19 12:17:06
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-03-19 12:35:47
 -->
[1][**理论资料**]: https://developer.mozilla.org/en-US/docs/Web/Performance/Critical_rendering_path

[2][**翻译自**]: https://bitsofco.de/understanding-the-critical-rendering-path/#4creatingtherendertree

## 什么是Critical Rendering Path

Critical Rendering Path, 以下简称CRP, 是浏览器将HTML，CSS和JavaScript转换为屏幕上的像素所经历的步骤序列。优化关键渲染路径可提高渲染性能。关键渲染路径包括文档对象模型（DOM），CSS对象模型（CSSOM），渲染树和布局。也就是我们平时大白话的那些疑问： 一个html资源，从请求到最终渲染完成，到底执行了哪些过程。

在解析HTML时会创建文档对象模型。HTML可以请求JavaScript，而JavaScript又可以更改DOM。HTML包含或发出样式请求，从而建立CSS对象模型。浏览器引擎将两者结合在一起以创建渲染树。布局确定页面上所有内容的大小和位置。确定布局后，将像素绘制到屏幕上。

优化CRP可以缩短首次渲染的时间。了解和优化CRP对于确保重排和重绘可以每秒60帧的速度进行，以确保高效的用户交互并避免产生麻烦是至关重要的。

## 理解CRP

Web性能包括服务器请求和响应，加载，脚本编写，渲染，布局以及将像素绘制到屏幕上。

对网页或应用程序的请求以HTML请求开头。服务器返回HTML-响应标头和数据。然后，浏览器开始解析HTML，将接收到的字节转换为DOM树。浏览器在每次找到到外部资源的链接时都会发起请求，这些链接可以是样式表，脚本或嵌入式图像引用。某些请求被阻止，这意味着将停止其余HTML的解析，直到处理了导入的资产为止。浏览器将继续解析HTML发出的请求并构建DOM，直到到达终点为止，此时将构造CSS对象模型。完成DOM和CSSOM后，浏览器将构建渲染树，计算所有可见内容的样式。渲染树完成后，将进行布局，以定义所有渲染树元素的位置和大小。

```html
<html>
    <head>
    <title>Understanding the Critical Rendering Path</title>
    <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <header>
            <h1>Understanding the Critical Rendering Path</h1>
        </header>
        <main>
            <h2>Introduction</h2>
            <p>Lorem ipsum dolor sit amet</p>
        </main>
        <footer>
            <small>Copyright 2017</small>
        </footer>
    </body>
</html>
```

当浏览器在渲染一个html文件的时候，有几个步骤需要先执行，这个过程我们叫做 “ 关键路径渲染 ”（CRP）。

我们在掌握或者了解CRP知识的前提下，可以去合理的提升性能。关于CRP，有6点需要掌握：

* 构造DOM树 

* 构造CSSOM树 

* 运行JS 

* 创建渲染树 

* 创建布局（Layout  ） 

*  绘制 

<ims src="https://bitsofco.de/content/images/2017/01/CRP-Sequence-Copy.png">

#### 构建DOM树

DOM树，即一个html文件的所有DOM节点的对象,结构为树形结构，DOM构建是增量的。HTML响应变成令牌，令牌变成节点，而节点又变成DOM树。单个DOM节点以startTag令牌开始，以endTag令牌结束。节点包含有关HTML元素的所有相关信息。该信息是使用令牌描述的。节点根据令牌层次结构连接到DOM树中。如果另一组startTag和endTag令牌位于一组startTag和endTag之间，则您在节点内有一个节点，这就是我们定义DOM树层次结构的方式。

节点数量越多，关键渲染路径中的后续事件将花费的时间就越长

<ims src="https://bitsofco.de/content/images/2017/01/DOM.png">

dom节点的渲染是可以分部执行的，浏览器不用等到整个html结构渲染完再去绘制，我们可以看到优先渲染出来的元素，但是css和js会打断dom结构的渲染。

#### 渲染CSSOM树

CSSOM 是 样式和DOM的关系映射对象，和DOM很像

```css
body { font-size: 18px; }

header { color: plum; }
h1 { font-size: 28px; }

main { color: firebrick; }
h2 { font-size: 20px; }

footer { display: none; }
```

以上CSS生成的CSSOM如下

<img src="https://bitsofco.de/content/images/2017/01/CSSOM.png">

CSSOM被成为"渲染阻塞资源"，由于css本身的层叠与继承特性，后定义的样式会覆盖之前的样式，如果我们使用不当会造成过多的dom渲染的阻塞（DOM元素最终长什么样子还由css决定），因此css不能分部执行，我们可以一次性加载完所有的css，这对性能是有好处的。

DOM包含页面的所有内容。CSSOM包含页面的所有样式。有关如何设置DOM样式的信息。CSSOM与DOM类似，但有所不同。虽然DOM构造是增量的，但CSSOM不是。CSS被阻止渲染：浏览器将阻止页面渲染，直到它接收并处理所有CSS。CSS是渲染阻止的，因为规则可以被覆盖，因此在CSSOM完成之前无法渲染内容。

CSS有自己的一组规则来标识有效令牌。记住CSS中的C代表“ Cascade”。CSS规则逐渐降低。解析器将令牌转换为节点，节点的后代继承样式。增量处理功能不适用于CSS，就像HTML一样，因为后续规则可能会覆盖先前的规则。CSS对象模型是在CSS解析时生成的，但是在完全解析它之前不能用于构建渲染树，因为不应在屏幕上渲染将被以后的解析覆盖的样式。

在选择器性能方面，较不特定的选择器比较特定的选择器更快。例如，.foo {}速度比速度快，.bar .foo {}因为.foo在第二种情况下，当浏览器找到时，它必须走到DOM上以检查是否.foo有祖先.bar。更具体的标签需要浏览器进行更多工作，但是这种损失可能不值得优化。

如果您测量解析CSS所花费的时间，您将对浏览器真正的速度感到惊讶。更具体的规则更昂贵，因为它必须遍历DOM树中的更多节点-但是额外的费用通常很少。首先测量。根据需要进行优化。特异性可能不是您的低谷果。说到CSS，优化选择器性能，改进仅需几微秒。还有其他优化CSS的方法，例如最小化，以及使用媒体查询将延迟的CSS分离为非阻塞请求。

css不止可以阻断dom的渲染，也可以阻断js的运行。

#### 运行时的JS

Js被成为“解析器阻塞资源”，也就是DOM结构在解析的时候会被js阻塞

我们可以避免阻塞，使用script标签的async属性

#### 创建渲染树

渲染树是DOM和CSSOM的结合，同时捕获内容和样式：DOM和CSSOM树被合并到渲染树中。为了构建渲染树，浏览器从DOM树的根开始检查每个节点，并确定要附加哪些CSS规则。是最终视图效果的呈现（ 也就是说不包括display:none的效果 ）

<img src="https://bitsofco.de/content/images/2017/01/Render-Tree.png">

#### 生成Layout

Layout 决定视图窗口的大小，提供CSS样式中百分比或窗口单位的上下文，Layout通过meta标签设定。

构建渲染树后，就可以进行布局。布局取决于屏幕的大小。布局步骤确定元素在页面上的位置和方式，确定每个元素的宽度和高度，以及它们之间的相对关系。

元素的宽度是多少？根据定义，块级元素的默认宽度为其父级元素的宽度的100％。宽度为50％的元素将为其父元素的宽度的一半。除非另有定义，否则主体的宽度为100％，这意味着它将是视口宽度的100％。设备的此宽度会影响布局。

视口元标记定义布局视口的宽度，从而影响布局。没有它，浏览器将使用默认的视口宽度，默认情况下，全屏浏览器的视口宽度通常为960px。在默认的全屏浏览器（如手机的浏览器）上，通过设置<meta name="viewport" content="width=device-width">，宽度将是设备的宽度，而不是默认的视口宽度。当用户在横向和纵向模式之间旋转手机时，设备宽度会发生变化。每次旋转设备或调整浏览器大小时都会发生布局。

布局性能受DOM影响-节点数量越多，布局所需的时间就越长。布局可能会成为瓶颈，如果在滚动或其他动画过程中需要的话，可能会导致混乱。加载或方向更改时20ms的延迟可能很好，但会导致动画或滚动出现颠簸。每当修改渲染树时（例如，通过添加节点，更改内容或更新节点上的盒子模型样式），都会发生布局。

为了减少布局事件的频率和持续时间，请批量更新并避免对盒模型属性进行动画处理。

```html
<meta name="viewport" content="width=device-width,initial-scale=1">
```

#### 绘画 ( 填充像素 )

最终浏览器将视图进行绘画。且绘画的时间长短取决于DOM节点的大小和样式的多少。例如： 一个复杂的背景图片的绘画肯定要比一个简单背景图片的绘画耗时长。

<img src="https://bitsofco.de/content/images/2017/01/Timeline.png">

对上图做说明，CRP的步骤如下
* 发送请求 - GET request sent for index.html
* 解析HTML并发送请求 - Begin parsing of HTML and DOM construction. Send GET request for style.css and main.js
* 解析样式 - CSSOM created for style.css
* 运行JS脚本 - Evaluate main.js
* 布局 - Generate Layout based on meta viewport tag in HTML
* 绘画 - Paint pixels on document

## 针对CRP进行优化

通过优先确定要加载的资源，控制加载的顺序以及减小这些资源的文件大小，来提高页面加载速度。性能提示包括：

* 通过延迟关键资源的下载，将它们标记为异步或完全消除它们来最大程度地减少关键资源的数量；

* 优化所需的请求数量以及每个请求的文件大小; （HTTP2.0 下无需如此） 

* 优化顺序通过优先下载关键资产来加载哪些关键资源，缩短关键路径长度
