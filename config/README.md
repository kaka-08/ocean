**开门见山，步骤如下**

# 如何在gitlab搭建一个静态站点

0. 在gitlab创建一个项目

1. 进入创建好的项目当中，设置 CI/CD ( 点击 Set up CI/CD )

2. 设置CI/CD也就是填写 .gitlab-ci.yml, 此时处于 .gitlab-ci.yml的编辑页面，可以选择 .gitlab-ci.yml 模版，此时配置内容会自动生成, 如果这里不知道怎么选，不着急，继续往下看

3. https://gitlab.com/pages ，这个页面提供了很多demo, 由于我搭建的是gitbook项目，所以我copy的是对应的gitbook项目的配置，你们也可以用别的，例如Hexo等等,

4. 上面步骤执行完之后，此时就可以 尝试 gitbook了，可以将上面选择的demo原封不动的copy到你的项目下，或者可以在本地 gitbook init一个项目。然后 push到远程。记住： README.md 和 .gitlab-ci.yml 都在项目根目录下

5. 最后一步: 在gitlab上 项目当中，点击Setting，进入Pages页面，会看到这么一句话： 
> With GitLab Pages you can host your static websites on GitLab. Combined with the power of GitLab CI and the help of GitLab Runner you can deploy static pages for your individual projects, your user or your group. 且默认是勾选  Force HTTPS (requires valid certificates)的。

   点击Save按钮。。然后刷新页面（ 可能响应不及时 ），会提示你的静态站点已经配置好，需要等待30分钟，并且会显示访问地址。然后大概30分钟之后就可以访问你的静态站点了 


# 本地开发

1. 可以创建book.json文件配置你的博客，具体可以查看这篇文章: http://www.chengweiyang.cn/gitbook/customize/book.json.html

2. 本地执行 
   ```sh
      gitbook install && gitbook serve 
   ```
   可以预览效果

3. 编辑完成，只需要 提交代码即可， gitlab会自动部署项目 ，部署时间大概2分钟 ,进度可去项目的CI/CD目录下查看，例如你的站点名叫 demo， 地址大概是： https://gitlab.com/用户名/项目名/pipelines


