<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-03-11 13:53:15
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-03-19 11:57:44
 -->

# Superset 二次开发 加入地图

* 接到需求的时候我和你一样懵。。。什么是Superset ??? 如何二次开发 ???

我们先来认识一下 Superset
>Apache Superset (incubating) is a modern, enterprise-ready business intelligence web application

关键词: enterprise-ready business 、 intelligence、 application 
这似乎不能解释他到底是什么 ，继续

>Apache Superset is a data exploration and visualization web application.

关键词：visualization（可视化）应用

看几个Superset官网的效果图片

<img src="https://raw.githubusercontent.com/apache/incubator-superset/master/superset-frontend/images/screenshots/bank_dash.png">
<img src="https://raw.githubusercontent.com/apache/incubator-superset/master/superset-frontend/images/screenshots/explore.png">
<img src="https://raw.githubusercontent.com/apache/incubator-superset/master/superset-frontend/images/screenshots/sqllab.png">
<img src="https://raw.githubusercontent.com/apache/incubator-superset/master/superset-frontend/images/screenshots/deckgl_dash.png">

Superset 的大致功能和说明:

* 支持大多数的数据库

* 允许通过定义维度和指标来控制数据展示

* 用于浏览和可视化数据集，并创建交互式仪面板

* 面板由切片组成，各个切片可以灵活组装成面板

* 开源的

* 等等

一句话： Superset 是开源的、智能的可视化应用，您可以通过各种指标和查询配置，将数据库的数据进行可视化，无需开发代码。

* Superset 是使用了D3.js，React，Python开发。这个阅读源码可知。

## 开发前的准备工作

* 准备IDE,我使用的是 PyCharm + VSCode, 大家按照自己的开发习惯定

* 访问 https://github.com/apache/incubator-superset （ superset项目地址 ）（ 目前最新版本是 0.36,我们可以切换到0.34分支，0.34之后，Superset将前端和后端代码拆分了，结构变化的很快。**0.28版本及之前的项目当中有superset-fontend包，这个包内容大多数是前端可视化插件源码，此后的版本当中移除了，且将这些源码发布到了superset-ui，改动较大** ），假设项目的目录是 superset-demo

```
  cd superset-demo
  
```

* 配置环境，安装依赖，我参考的是官网 https://superset.incubator.apache.org/installation.html#getting-started。支持Docker 和 virtualenv，我选择的是后者,**先在本地装Python3,Python3的安装和配置这里就不说了哈**
>It is recommended to install Superset inside a virtualenv. Python 3 already ships virtualenv. But if it’s not installed in your environment for some reason, you can install it via the package for your operating systems, otherwise you can install from pip:

安装虚拟环境
```
pip install virtualenv 
```

在superset-demo下，创建虚拟环境,这里名字叫 vdev

```
    python3 -m venv vdev
```

开启（ 运行 ）虚拟环境  

```
  . vdev/bin/activate

```

在虚拟环境下安装python配置 
```
pip install --upgrade setuptools pip
```

安装数据库 ,一定要安装数据库客户端，否则项目会无法启动，如果项目最终启动过程报 mysql_config: not found ，就是说明mysqlclient安装或者配置有问题
```
pip3 install mysqlclient
```

安装Superset，安装之后可以使用 superset 命令
```
pip install apache-superset
```

执行命令 初始化数据库
```
superset db upgrade
```

创建管理员（Create an admin user (you will be prompted to set a username, first and last name before setting a password)）

```
export FLASK_APP=superset
flask fab create-admin
```

展示某些demo ( 非必要 )
```
superset load_examples
```

初始化项目，创建默认角色和权限，这个无需太关注，照着做就好（Create default roles and permissions）
```
superset init
```

启动项目,启动之后 浏览器访问 http://localhost:8088 即可
```
superset run -p 8088 --with-threads --reload --debugger
```

退出项目
```
deactivate
```


## 当启动项目之后

Superset的前后端项目是耦合在一起的，
前端的位置为（ 上面我们创建的文件夹为 superset_demo ）

```
superset_demo/vdev/lib/python3.7/site-packages/superset/static/asset
```
<img src="https://feat-assets.oss-cn-beijing.aliyuncs.com/aet/WechatIMG53.png">

我们需要进入到前端目录下执行我们熟悉的那些操作,启动的命令查看 package.json 的 scripts配置
```
cd asset
cnpm i
npm run dev

```

**我习惯于使用VSCode启动前端，PyCharm启动后端，这样方便开发，这就是为什么上面要准备两个IDE**

## 二次开发

在这之前可以先读一下**VizTypeControl.js**源码，可以找到上下文关系，我下面就直接说明了，省的耽误大家时间


以下是二次开发需要知道的一些文件

* incubator-superset/superset/assets/src/visualizations 文件夹： 存储可视化插件
<img src="https://feat-assets.oss-cn-beijing.aliyuncs.com/aet/superset001.png" width=500>

插件大多数都是基于D3.js开发，之前研究过一段时间D3。后来项目当中没有使用D3.js的地方，就生疏了。😄

* 以FilterBox插件为例，目录结构如下
  + images： 文件夹，存储插件图片，展示在插件列表中
  + FilterBox.css： css文件
  + FilterBox.jsx： jsx文件，也就是插件具体的展示元素文件，React编写，大部分工作量都在这里，比方说展示地图等等
  + FilterBoxChartPlugin.jsx： 继承一些基类，然后封装属性，并且导出，作为我们引入插件的入口文件，可以读读源码看看
  + transformProps.js： 顾名思义，转换props，转换之后的props再丢给FilterBox

此处丢一下我自己的插件当中 transformProps.js内容
```js
export default function transformProps(chartProps) {
    const { width, height, payload } = chartProps;
      return {
          data: payload.data, // 请求sql返回的数据，其实还有别的，superset本身集成的数据
          height, //将高度丢进来，因为地图插件需要有初始化高度
      };
  }
```
**如果我们暴力一些，可以直接copy这个demo文件，改成别的插件名字，然后一个全新的插件就生成了😄😄😄，当然，还要做完接下来这些必要配置,以下假设我们自己的插件叫 DemoMap**


* incubator-superset/superset/assets/src/visualizations/presets/MainPreset.js：注册插件，我们按照格式引入自己的插件，并且注册一下即可

```JS
import DemoMapPlugin from '../DemoMap/DemoMapPlugin';
// .......
new DemoMapPlugin().configure({ key: 'demo_map' }),   // 当前名字不能取的这么随意哈 

```

* incubator-superset/superset/assets/src/explore/controlPanels/ 文件夹： 我们关注一下 index.js文件
(我们在这个目录下可以找到上面的FilterBox.js，此处的FilterBox.js的作用是配置sql查询的参数和条件),我们仿照新建自己的DemoMap.js
<img src="https://feat-assets.oss-cn-beijing.aliyuncs.com/aet/superset003.png" width={350}>

上图为此处DemoMap.js的作用

```js
//引入配置
import DemoMap from './DemoMap'; 
//导出配置
export const controlPanelConfigs = extraOverrides({
    ...
    demo_map: DemoMap,
    ...
})

```

* incubator-superset/superset/viz.py 可视化插件类集合，我们如果新增插件，就要在这里注册一个新的插件类

```python
class DemoMap(BaseViz):
    # viz_type要是自己注册的插件的名字
    viz_type = 'demo_map'
    is_timeseries = False

    # 这里大家自定义
    def get_data(self, df):
        form_data = self.form_data
        df.sort_values(by=df.columns[0], inplace=True)
        return df.to_dict(orient='records')
```

至此，完成, 列表中可以看到自定义的插件

<img src="https://feat-assets.oss-cn-beijing.aliyuncs.com/aet/superset004.png" width={600}>

最终展示
<img src="https://feat-assets.oss-cn-beijing.aliyuncs.com/aet/superset002.png" width=800}>

核心代码 DemoMap.js,由于涉及到业务，我只留了静态代码，大家看得懂就行

```js
/* eslint-disable no-nested-ternary */
/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
/* eslint-disable react/prop-types */
/* eslint-disable no-loop-func */
/* eslint-disable no-useless-concat */
/* eslint-disable react/sort-comp */
import { Scene, Marker, MarkerLayer } from '@antv/l7';  //依赖的地图插件 
import { GaodeMap } from '@antv/l7-maps'; //依赖的地图插件 
import React from 'react';
import './DemoMap.css';

export default class DemoMap extends React.Component {


    scene = null;

    state={
        info: {},
    }

    addMarkers=(data = []) => {
        const markerLayer = new MarkerLayer();
        for (let i = 0; i < data.length; i++) {
            const el = document.createElement('label');
            el.className = 'labelclass';
            el.style.background = this.getColor(data[i].count);
            el.style.borderColor = this.getColor(data[i].count);
            const marker = new Marker({
                  element: el,
                  extData: data[i],
                }).setLnglat({
                    lng: data[i].longitude * 1,
                    lat: data[i].latitude,
                });
                // 这里根据自己的业务处理哈 TODO
                marker.on('click', (e) => {
                    console.log('e %o', e);
                });
            markerLayer.addMarker(marker);
        }
        this.scene.addMarkerLayer(markerLayer);
    }

    getColor=(v) => {
      const colors = ['#ffffe5', '#f7fcb9', '#d9f0a3', '#addd8e', '#78c679', '#41ab5d', '#238443', '#005a32'];
      return v > 350
        ? colors[7]
        : v > 90
          ? colors[6]
          : v > 30
            ? colors[5]
            : v > 20
              ? colors[4]
              : v > 10
                ? colors[3]
                : v > 5
                  ? colors[2]
                  : v > 0
                    ? colors[1]
                    : colors[0];
    }

    componentDidMount() {
      const scene = new Scene({
          id: 'map',
          map: new GaodeMap({
          center: [96.403981, 39.114935],
          style: 'dark',
          pitch: 0,
          zoom: 4.06,
        }),
      });

      //注意：⭐️⭐️ 这里的data 可以从 props当中获取，实际 数据是从sql当中查询得出
      const data = [{
            id: '5011000000404',
            name: '太原',
            longitude: 112.549717,
            latitude: 37.87046,
            unit_price: 71469.4,
            count: 100,
          },
          {
            id: '5011000000402',
            name: '长治',
            longitude: 113.113556,
            latitude: 36.191112,
            unit_price: 71469.4,
            count: 30,
          },
          {
            id: '5011000000402',
            name: '北京',
            longitude: 116.407387,
            latitude: 39.904179,
            unit_price: 71469.4,
            count: 500,
          },
        ];

      this.scene = scene;

      this.addMarkers(data);

      // 地图加载有延时
      setTimeout(() => {
          this.scene.setMapStyle('amap://styles/2a09079c3daac9420ee53b67307a8006?isPublic=true');
          this.scene.on('click', (ev) => {
            const { lnglat } = ev;
            this.getProvinceData(lnglat.lng, lnglat.lat);
          });
      }, 1000);
    }

    componentWillUnmount() {
        this.scene.destroy();
    }

    render() {
        const { info = {} } = this.state; // TODO
        const { height } = this.props; // ⭐️⭐️ 这里的height从transformProps.js当中获取，大家可以联想别的props
        return (
          <div className="main">
            <div className="info-panel">
              <div className="city-wrap">
                <h1 className="province-title">太原市</h1>
                <div className="open-province-time">
                  <p className="tip">加盟时间：</p>
                  <i className="time">2019-10-10</i>
                </div>
              </div>
              <div className="city-info">
                <h1 className="city-title">总利润</h1>
                <p className="mount"><span>190,010,213</span> 元</p>
                <div className="spec">
                  <div className="spec-item">
                    <h1>昨日订单量</h1>
                    <p><span>674018</span> 单</p>
                  </div>
                  <div className="spec-item">
                    <h1>市级店铺</h1>
                    <p><span>71</span> 座</p>
                  </div>
                </div>
                <div className="item">
                  <div className="label">
                    <p className="title">已服务用户数量</p>
                    <p className="unit">单位（个）</p>
                  </div>
                  <span className="count">109123</span>
                </div>
                <div className="item">
                  <div className="label">
                    <p className="title">总人口数</p>
                    <p className="unit">单位（个）</p>
                  </div>
                  <span className="count">321231</span>
                </div>
                <div className="item">
                  <div className="label">
                    <p className="title">已服务用户率</p>
                    <p className="unit">单位（%）</p>
                  </div>
                  <span className="count">33.97 %</span>
                </div>
              </div>
              <div id="map" style={{ minWidth: 500, height }} />
            </div>
          </div>
        );
    }
}

```
