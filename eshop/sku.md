<!--
 * @Description: 
 * @version: 
 * @Author: zwj
 * @Date: 2020-03-11 12:02:12
 * @LastEditors: sueRimn
 * @LastEditTime: 2020-03-17 16:51:13
 -->
# sku的设计思路

**sku的概念，来源于知乎**

https://www.zhihu.com/question/19841574

**传统模式下，商品的入库流程**
* 今日入库的A品 和 昨日录入的A品，要区别对待，方便统计

* 模版的作用: 不同时期录入的同样商品，模版数据相同，仅仅是生产日期 + 进价不同，不必要每次都录入一堆重复属性

* 商品 和 模版关系为 一对一，模版 和 sku 关系为 一对多

* 仓储系统存储信息 越细越好

<img src='https://feat-assets.oss-cn-beijing.aliyuncs.com/aet/skus.png' width=1000>